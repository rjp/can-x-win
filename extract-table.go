package main

import (
    "fmt"
    "flag"
    "strings"

_    "github.com/PuerkitoBio/goquery"
)

type teamData []string
type table []teamData

func main() {
    var leagueType = flag.String("type", "bbc", "Type of league: bbc, soccerstats")
    flag.Parse()

    var doit extractInterface
    if *leagueType == "bbc" {
        doit = bbc{}
    }
    if *leagueType == "soccerstats" {
        doit = soccerstats{}
    }
    if *leagueType == "soccerstats2" {
        doit = soccerstats2{}
    }

    // table := soccerstats(doc)
    league := doit.league()

    for _, team := range league {
        fmt.Println(strings.Join(team, "\t"))
    }
}
