package main

import (
    "strconv"
    "reflect"
)

type FieldMismatch struct {
	expected, found int
}

func (e *FieldMismatch) Error() string {
	return "CSV line fields mismatch. Expected " + strconv.Itoa(e.expected) + " found " + strconv.Itoa(e.found)
}

type UnsupportedType struct {
	Type string
}

func (e *UnsupportedType) Error() string {
	return "Unsupported type: " + e.Type
}

func Unmarshal(fields []string, v interface{}) error {
    s := reflect.ValueOf(v).Elem()
    if s.NumField() != len(fields) {
        return &FieldMismatch{s.NumField(), len(fields)}
    }
    for i := 0; i < s.NumField(); i++ {
        f := s.Field(i)
        switch f.Type().String() {
        case "string":
            f.SetString(fields[i])
        case "int":
            ival, err := strconv.ParseInt(fields[i], 10, 0)
            if err != nil {
                return err
            }
            f.SetInt(ival)
        default:
            return &UnsupportedType{f.Type().String()}
        }
    }
    return nil
}
