F=${FFILE:-data/f}
T=${TFILE:-data/t}
L="lose"
 awk -F "\t" '{print $2}' $T | while read i; do j=${i// /_}; ./run-simulation -team "$i" -table $T -fixtures $F -type $L -undraws | grep "xx .. $j" | tail -1; done | sort -rn -k 11 | sed -e 's/^xx //'
