package main

import (
    "fmt"
    "strings"

    "github.com/PuerkitoBio/goquery"
)

type soccerstats2 struct {}

func (q soccerstats2) leagueURL() string {
    return "http://www.soccerstats.com/latest.asp?league=england2"
}

func (q soccerstats2) fixturesURL() string {
    return "http://www.soccerstats.com/results.asp?league=england2"
}

func (q soccerstats2) fixtures() {
    doc := q.getData(q.fixturesURL())
    doc.Find(".row .nine table tr.odd").Each(func(i int, row *goquery.Selection) {
        var tmp []string
        row.Find("td").Each(func(i int, cell *goquery.Selection) {
            tmp = append(tmp, strings.TrimSpace(cell.Text()))
        })
        score := tmp[3]
        teams := strings.Split(tmp[2], " - ")
//        fmt.Println(teams,"||",score)
        home, away := teams[0], teams[1]
        if score ==  "-" || score == "pp." || score == "" {
			ts := []string{home, away, "11:22"}
            fmt.Println(strings.Join(ts, "\t"))
        }
    })
}

func (q soccerstats2) getData(url string) *goquery.Document {
    doc, err := goquery.NewDocument(url)
    if err != nil { panic(err) }

    return doc
}

func (q soccerstats2) league() table {
    doc := q.getData(q.leagueURL())
    league := table{}
    doc.Find(".seven #btable .trow8").Each(func(i int, row *goquery.Selection) {
        tmp := teamData{}
        row.Find("td").Slice(0,10).Each(func(i int, cell *goquery.Selection) {
            tmp = append(tmp, strings.TrimSpace(cell.Text()))
        })
        league = append(league, tmp)
    })
    return league
}

