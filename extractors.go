package main

import (
)

type extractInterface interface {
    leagueURL() string
    league() table
    fixturesURL() string
    fixtures()
}
