package main

import (
    "fmt"
    _ "os"
    "flag"
    "sort"
    "strings"
    "io/ioutil"
)

var myteam string
var fixturesFile string
var tableFile string
var verbose bool
var direction string
var gamesLimit int
var undraws bool

func doFlags() {
    flag.BoolVar(&verbose, "verbose", false, "Whether to print the table each fixture")
    flag.StringVar(&myteam, "team", "Everton", "Which team to simulate")
    flag.StringVar(&fixturesFile, "fixtures", "data/f", "File containing fixtures")
    flag.StringVar(&tableFile, "table", "data/t", "File containing current table")
    flag.StringVar(&direction, "type", "win", "Whether to [win] or [lose]")
    flag.IntVar(&gamesLimit, "games", 0, "After how many games")
    flag.BoolVar(&undraws, "undraws", false, "Whether above-above should be d-d")

    flag.Parse()

    if direction != "win" && direction != "lose" {
        direction = "win"
    }
}

type fixture struct {
    home string
    away string
}

type teamRecord struct {
    Name string
    Played int
    Won int
    Drawn int
    Lost int
    GoalsFor int
    GoalsAgainst int
    Points int
    Pos int
}

type leaguePosToTeam map[string]int
type actualLeagueTable []*teamRecord
type season []fixture

type leagueTable struct {
    pos leaguePosToTeam
    table actualLeagueTable
}

type simulation struct {
    league leagueTable
    myTeam string
    maxPoints int
    fixtures season
}

func (s actualLeagueTable) Len() int { return len(s) }
func (s actualLeagueTable) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s actualLeagueTable) Less(i, j int) bool {
    if s[i].Points == s[j].Points {
        if s[i].Name == myteam {
            return false
        }
        if s[j].Name == myteam {
            return true
        }
        hGD := s[i].GoalsFor - s[i].GoalsAgainst
        aGD := s[j].GoalsFor - s[j].GoalsAgainst
        return hGD > aGD
    }
    return (s[i].Points > s[j].Points)
}

func (self simulation) playWinningFixture(match fixture) {
    // Need these as strings because I have't converted all
    // the processing functions below to take `*teamRecord`
    // which would be more sensible. Maybe.
    home, away := match.home, match.away
        md := self.ptrToTeam(self.myTeam)
        hd := self.ptrToTeam(home)
        ad := self.ptrToTeam(away)
        fmt.Println(hd, ad)
        fmt.Println(md)

        if home == myteam {
            fmt.Println("Automatic home loss for", myteam)
            self.league.loseFor(away)
            self.league.winFor(home)
            return
        }

        // We automatically win away
        if away == myteam {
            fmt.Println("Automatic away loss for", myteam)
            self.league.loseFor(home)
            self.league.winFor(away)
            return
        }

        // If we can't reach the home team ...
        if hd.Points > self.maxPoints {
            if ad.Points > self.maxPoints { // ... and we can't reach the away team, they can draw.
                fmt.Println(home,"(unreachable) d-d (unreachable", ad.Points,")", away)
                self.league.drawFor(home, away)
            } else { // ... but we can reach the away team, they lose
                fmt.Println(home,"(unreachable) w-l (reachable)", away)
                self.league.loseFor(away)
                self.league.winFor(home)
            }
            return
        }

        // Same but the other way around
        if ad.Points > self.maxPoints {
            if hd.Points > self.maxPoints {
                fmt.Println(home,"(unreachable) d-d (unreachable)", away)
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home,"(reachable) l-w (unreachable)", away)
                self.league.loseFor(home)
                self.league.winFor(away)
            }
            return
        }

        if hd.Pos < md.Pos {
            if ad.Pos <= md.Pos {
                if undraws {
                    if hd.Pos < ad.Pos {
                        fmt.Println(home,"(above^) l-w (above)",away)
                        self.league.loseFor(home)
                        self.league.winFor(away)
                    } else {
                        fmt.Println(home,"(above) w-l (above^)",away)
                        self.league.loseFor(away)
                        self.league.winFor(home)
                    }
                } else {
                    fmt.Println(home,"(above) d-d (above)", away)
                    self.league.drawFor(home, away)
                }
            } else {
                fmt.Println(home,"(above) l-w (below)", away)
                self.league.loseFor(home)
                self.league.winFor(away)
            }
            return
        }

        if ad.Pos < md.Pos {
            if hd.Pos <= md.Pos {
                fmt.Println(home,"(above) d-d (above)", away)
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home,"(below) w-l (above)", away)
                self.league.loseFor(away)
                self.league.winFor(home)
            }
            return
        }
        if hd.Pos > md.Pos {
            if hd.Pos > ad.Pos {
                self.league.loseFor(away)
                self.league.winFor(home)
                fmt.Println(home, "l-w", away, "because away closest")
            } else {
                self.league.loseFor(home)
                self.league.winFor(away)
                fmt.Println(home, "w-l", away, "because home closest")
            }
            return
        }

        if ad.Pos > md.Pos {
            if ad.Points + 4 < md.Points {
                fmt.Println(home, "l-w", away, "because still below", myteam)
                self.league.loseFor(away)
                self.league.winFor(home)
            } else {
                fmt.Println(home, "d-d", away, "#2")
                self.league.drawFor(home, away)
            }
            return
        }

        if hd.Pos > md.Pos - 3 {
            if ad.Pos < md.Pos - 4 {
                fmt.Println(home, "d-d", away, "both close")
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home, "l-w", away, "(home close)")
                self.league.loseFor(home)
                self.league.winFor(away)
            }
            return
        }

        if ad.Pos > md.Pos - 3 {
            if hd.Pos < md.Pos - 3 {
                fmt.Println(home, "d-d", away, "both close")
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home, "w-l", away, "(home close)")
                self.league.loseFor(away)
                self.league.winFor(home)
            }
            return
        }

        if hd.Pos < 6 {
            self.league.loseFor(away)
            self.league.winFor(home)
            return
        } else if ad.Pos < 6 {
            self.league.loseFor(home)
            self.league.winFor(away)
            return
        } else {
            self.league.drawFor(home, away)
        }
}

func (self simulation) playLosingFixture(match fixture) {
    // Need these as strings because I have't converted all
    // the processing functions below to take `*teamRecord`
    // which would be more sensible. Maybe.
    home, away := match.home, match.away
        md := self.ptrToTeam(self.myTeam)
        hd := self.ptrToTeam(home)
        ad := self.ptrToTeam(away)
        fmt.Println(hd, ad)
        fmt.Println(md)

        if match.home == myteam {
            fmt.Println("Automatic home loss for", myteam)
            self.league.winFor(away)
            self.league.loseFor(home)
            return
        }

        // We automatically win away
        if away == myteam {
            fmt.Println("Automatic away loss for", myteam)
            self.league.winFor(home)
            self.league.loseFor(away)
            return
        }

        // If we can't reach the home team ...
        if hd.Points > self.maxPoints {
            if ad.Points > self.maxPoints { // ... and we can't reach the away team, they can draw.
                fmt.Println(home,"(unreachable) d-d (unreachable", ad.Points,")", away)
                self.league.drawFor(home, away)
            } else { // ... but we can reach the away team, they lose
                fmt.Println(home,"(unreachable) w-l (reachable)", away)
                self.league.winFor(away)
                self.league.loseFor(home)
            }
            return
        }

        // Same but the other way around
        if ad.Points > self.maxPoints {
            if hd.Points > self.maxPoints {
                fmt.Println(home,"(unreachable) d-d (unreachable)", away)
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home,"(reachable) l-w (unreachable)", away)
                self.league.winFor(home)
                self.league.loseFor(away)
            }
            return
        }

        if hd.Pos < md.Pos {
            if ad.Pos <= md.Pos {
                fmt.Println(home,"(above) d-d (above)", away)
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home,"(above) l-w (below)", away)
                self.league.winFor(home)
                self.league.loseFor(away)
            }
            return
        }

        if ad.Pos < md.Pos {
            if hd.Pos <= md.Pos {
                fmt.Println(home,"(above) d-d (above)", away)
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home,"(below) w-l (above)", away)
                self.league.winFor(away)
                self.league.loseFor(home)
            }
            return
        }
        if hd.Pos > md.Pos {
            if hd.Pos > ad.Pos {
                self.league.winFor(away)
                self.league.loseFor(home)
                fmt.Println(home, "l-w", away, "because away closest")
            } else {
                self.league.winFor(home)
                self.league.loseFor(away)
                fmt.Println(home, "w-l", away, "because home closest")
            }
            return
        }

        if ad.Pos > md.Pos {
            if ad.Points + 4 < md.Points {
                fmt.Println(home, "l-w", away, "because still below", myteam)
                self.league.winFor(away)
                self.league.loseFor(home)
            } else {
                fmt.Println(home, "d-d", away, "#2")
                self.league.drawFor(home, away)
            }
            return
        }

        if hd.Pos > md.Pos - 3 {
            if ad.Pos < md.Pos - 4 {
                fmt.Println(home, "d-d", away, "both close")
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home, "l-w", away, "(home close)")
                self.league.winFor(home)
                self.league.loseFor(away)
            }
            return
        }

        if ad.Pos > md.Pos - 3 {
            if hd.Pos < md.Pos - 3 {
                fmt.Println(home, "d-d", away, "both close")
                self.league.drawFor(home, away)
            } else {
                fmt.Println(home, "w-l", away, "(home close)")
                self.league.winFor(away)
                self.league.loseFor(home)
            }
            return
        }

        if hd.Pos < 6 {
            self.league.winFor(away)
            self.league.loseFor(home)
            return
        } else if ad.Pos < 6 {
            self.league.winFor(home)
            self.league.loseFor(away)
            return
        } else {
            self.league.drawFor(home, away)
        }
}

func (league leagueTable) showTable() {
    league.sortTable()

    fmt.Printf("xx %2s %-20s %2s %2s %2s %2s %3s %3s %3s %3s\n",
    "  ", "Team", "P", "W", "D", "L", "GF", "GA", "GD", "Pts")
    for i, z := range league.table {
        usName := strings.Replace(z.Name, " ", "_", -1)

        fmt.Printf("xx %2d %-20s %2d %2d %2d %2d %3d %3d %3d %3d\n",
            i+1, usName, z.Played, z.Won, z.Drawn, z.Lost,
            z.GoalsFor, z.GoalsAgainst, z.GoalsFor - z.GoalsAgainst,
            z.Points)
    }
}

func (league leagueTable) sortTable() {
    sort.Stable(league.table)
    for i, z := range league.table {
        t := z.Name
        league.pos[t] = i
        z.Pos = i
    }
}

func loadFixtures(filename string) season {
    dat, err := ioutil.ReadFile(filename)
    if err != nil { panic(err) }

    var s season

    data := string(dat)
    rows := strings.Split(data, "\n")
    for _, row := range rows {
        fields := strings.Split(row, "\t")
        if len(fields) > 1 {
            f := fixture{fields[0], fields[1]}
            s = append(s, f)
        }
    }
    return s
}

func (self simulation) ptrToTeam(t string) *teamRecord {
    pos := self.league.pos[t]
    return self.league.table[pos]
}

func (league leagueTable) ptrToTeam(t string) *teamRecord {
    pos := league.pos[t]
    return league.table[pos]
}

// Is it better to have this as a method on `leagueTable` or on `Team`?
// The team wins but it also affects the league.  We could have
// a parent `leagueTable` attached to each `Team` but that seems mad.
func (league leagueTable) winFor(team string) {
    pT := league.ptrToTeam(team)

    pT.Points = pT.Points + 3
    pT.Won = pT.Won + 1
    pT.Played = pT.Played + 1
    pT.GoalsFor = pT.GoalsFor + 1
}

func (league leagueTable) loseFor(team string) {
    pT := league.ptrToTeam(team)

    pT.Lost = pT.Lost + 1
    pT.Played = pT.Played + 1
    pT.GoalsAgainst = pT.GoalsAgainst + 1
}

func (league leagueTable) drawFor(home string, away string) {
    hT := league.ptrToTeam(home)

    hT.Points = hT.Points + 1
    hT.Drawn = hT.Drawn + 1
    hT.Played = hT.Played + 1

    aT := league.ptrToTeam(away)

    aT.Points = aT.Points + 1
    aT.Drawn = aT.Drawn + 1
    aT.Played = aT.Played + 1
}



func loadTable(filename string) actualLeagueTable {
    t := actualLeagueTable{}

    dat, err := ioutil.ReadFile(filename)
    if err != nil { panic(err) }

    data := string(dat)
    rows := strings.Split(data, "\n")
    for i, row := range rows {
        fields := strings.Split(row, "\t")
        if len(fields) > 1 {
            d := []string{ fields[1], fields[2], fields[3], fields[4], fields[5], fields[6], fields[7], fields[9], string(i), }
            var tm teamRecord
            Unmarshal(d, &tm)
            t = append(t, &tm)
        }
    }
    return t
}

func main() {
    doFlags()

    table := loadTable(tableFile)
    games := loadFixtures(fixturesFile)

    league := leagueTable{leaguePosToTeam{}, table, }
    league.sortTable()

    myteam = strings.Replace(myteam, "_", " ", -1)

    md := league.ptrToTeam(myteam)
    maxPoints := md.Points + 3*(38-md.Played)
    fmt.Println("Maximum for",myteam,"should be",maxPoints)

    mySim := simulation{league, myteam, maxPoints, games}

    gamesPlayed := 0
    /*
    fmt.Printf("%=v\n", league)
    os.Exit(0)
    */

    for _, match := range mySim.fixtures {
        if verbose {
            mySim.league.showTable()
        } else {
            mySim.league.sortTable()
        }

        if gamesLimit > 0 && gamesPlayed > gamesLimit {
            break
        }

        if direction == "win" {
            mySim.playWinningFixture(match)
        } else {
            mySim.playLosingFixture(match)
        }
    }

    league.showTable()
}
