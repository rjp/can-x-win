# "Can X win?"

An updated version of [canderbywin.rb][cde] - an experiment inspired
by a discussion in late 2007 about whether Derby County (having an
[astonishingly bad 2007-08 season][dc]) could still win the Premiership
from such an abysmal position.

This code can do both 'best case' and 'worst case' simulations which,
if combined as per `bestworst.sh`, gives you a nice span graph of
possible finish positions -

     1  7 [c###e..             ] Leicester_City
     1 10 [c###e.....          ] Arsenal
     1 10 [c###e.....          ] Tottenham
     1 12 [c###e.......        ] Manchester_City
     1 12 [c###e.......        ] Manchester_Utd
     1 13 [c###e........       ] West_Ham_Utd
     1 14 [c###e.........      ] Stoke_City
     1 15 [c###e..........     ] Chelsea
     1 16 [c###e...........    ] Bournemouth
     1 16 [c###e...........    ] Everton
     1 16 [c###e...........    ] Liverpool
     1 16 [c###e...........    ] Southampton
     1 17 [c###e............   ] Watford
     1 17 [c###e............   ] West_Bromwich
     1 18 [c###e............r  ] Crystal_Palace
     2 18 [ ###e............r  ] Swansea_City
     5 20 [    e............rrr] Newcastle_Utd
     7 20 [      ...........rrr] Norwich_City
     7 20 [      ...........rrr] Sunderland
    12 20 [           ......rrr] Aston_Villa

# Build

Build system uses [redo](https://github.com/apenwarr/redo).

[cde]: https://github.com/rjp/oldhacks/blob/master/9/canderbywin.rb
[dc]: https://en.wikipedia.org/wiki/2007–08_Derby_County_F.C._season
