package main

import (
    "strings"

    "github.com/PuerkitoBio/goquery"
)

type bbc struct {}

func (q bbc) leagueURL() string {
    return "http://www.bbc.com/sport/football/tables"
}

func (q bbc) fixturesURL() string {
    return "http://www.bbc.com/sport/football/fixtures/partial/competition-118996114?structureid=5"
}

func (q bbc) fixtures() {
    return
}

func (q bbc) getLeagueData() *goquery.Document {
    doc, err := goquery.NewDocument(q.leagueURL()) // "http://www.soccerstats.com/latest.asp?league=england")
    if err != nil { panic(err) }

    return doc
}

func (q bbc) league() table {
    doc := q.getLeagueData()
    league := table{}
    doc.Find("table.table-stats tbody tr.team").Each(func(i int, row *goquery.Selection) {
        ts := row.Find("td").Map(func(i int, cell *goquery.Selection) string {
            return strings.TrimSpace(cell.Text())
        })
        tmp := []string{"1"}
        // Have to append the slice here because we can't use an expanded
        // slice in a literal constructor.
        tmp = append(tmp, ts[2:11]...)
        league = append(league, tmp)
    })
    return league
}

