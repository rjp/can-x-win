package main

import (
    "fmt"
    "strings"

    "github.com/PuerkitoBio/goquery"
)

type soccerstats struct {}

func (q soccerstats) leagueURL() string {
    return "http://www.soccerstats.com/latest.asp?league=england"
}

func (q soccerstats) fixturesURL() string {
    return "http://www.soccerstats.com/team_results.asp?league=england&pmtype=bydate"
}

func (q soccerstats) fixtures() {
    doc := q.getData(q.fixturesURL())
    doc.Find(".row .six table tr.odd").Each(func(i int, row *goquery.Selection) {
        var tmp []string
        row.Find("td").Each(func(i int, cell *goquery.Selection) {
            tmp = append(tmp, strings.TrimSpace(cell.Text()))
        })
        score := tmp[3]
        teams := strings.Split(tmp[2], " - ")
        fmt.Println(teams,"||",score)
        home, away := teams[0], teams[1]
        if score ==  "-" || score == "pp." || score == "" {
			ts := []string{home, away, "11:22"}
            fmt.Println(strings.Join(ts, "\t"))
        }
    })
}

func (q soccerstats) getData(url string) *goquery.Document {
    doc, err := goquery.NewDocument(url)
    if err != nil { panic(err) }

    return doc
}

func (q soccerstats) league() table {
    doc := q.getData(q.leagueURL())
    league := table{}
    doc.Find("#content .seven > #btable tr.odd").Each(func(i int, row *goquery.Selection) {
        tmp := teamData{}
        row.Find("td").Slice(0,10).Each(func(i int, cell *goquery.Selection) {
            tmp = append(tmp, strings.TrimSpace(cell.Text()))
        })
        league = append(league, tmp)
    })
    return league
}

