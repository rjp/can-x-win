. ./builder.sh
if [ -f $2.od ]; then
    . ./$2.od
else
    DEPS="$2.go"
fi

redo-ifchange $DEPS
go build -ldflags "-s -X main.commit=${commit} -X main.builtAt='${built_at}' -X main.builtBy=${built_by} -X main.builtOn=${built_on}" -o $3 $DEPS

