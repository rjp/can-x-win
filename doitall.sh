l=$HOME/www/league.txt
type=${1:-soccerstats}
echo "Processing $type"
cd $HOME/git/can-x-win
./extract-table -type $type > data/t
./extract-fixtures -type $type > data/f
if [ -x "bestworst-$type.sh" ]; then
  ./bestworst-$type.sh | tee $l.new
else
  ./bestworst.sh | tee $l.new
fi
# && mv -f $l.new $l
